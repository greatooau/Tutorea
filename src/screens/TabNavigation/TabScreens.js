import MyProfile from "./tabScreens/MyProfile"
import MyTutors from "./tabScreens/MyTutors";
import Home from "./tabScreens/Home";
import EditProfile from "./tabScreens/EditProfile";
import ChangePassword from "./tabScreens/ChangePassword";
import PayInfo from "./tabScreens/PayInfo";
import DetailTutorNoPay from "./tabScreens/DetailTutorNoPay";
import DetailTutorPay from "./tabScreens/DetailTutorPay";
import Hiring from "./tabScreens/Hiring";
import Categories from "./tabScreens/Categories";
export { MyProfile, MyTutors, Home, EditProfile, ChangePassword, PayInfo, DetailTutorNoPay, DetailTutorPay, Hiring, Categories, }